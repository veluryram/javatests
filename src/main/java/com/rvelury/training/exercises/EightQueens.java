package com.rvelury.training.exercises;

public class EightQueens
{
    int boardSize = 2;
    boolean[][] board;
    int solutionCount=0;

    EightQueens(int boardSize)
    {
        this.boardSize = boardSize;
        board = new boolean[boardSize][boardSize];
    }
    public static void main(String[] args)
    {

        int boardSize = 8;

        if (args.length >  0)
        {
            boardSize = Integer.parseInt(args[0]);
        }

        EightQueens test = new EightQueens(boardSize);

        test.solve(0);
        System.out.println("Total Solutions: "+ test.solutionCount);

    }

    public void solve(int col)
    {
        if (col >= boardSize)
        {
            printSolution();
            solutionCount++;
            return;
        }

        for (int i = 0; i  < boardSize; i++)
        {
            if (!checkConflict(col, i))
            {
                board[col][i] = true;
                solve(col+1);
            }
            board[col][i] = false;
        }
    }

    boolean checkConflict(int col, int row)
    {
        for(int i=0; i < col; i++)
        {
           if (board[i][row])
               return true;

           if (row-col+i >= 0 && board[i][row-col+i])
               return true;

            if (row+col-i < boardSize && board[i][row+col-i])
                return true;
        }
        return false;
    }

    void printSolution()
    {
        for (int i = 0; i  < boardSize; i++)
        {
            for (int j = 0; j  < boardSize; j++)
            {
                System.out.print(board[j][i]?"1 ":"0 ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }
}
