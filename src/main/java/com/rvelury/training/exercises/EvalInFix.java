package com.rvelury.training.exercises;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

enum State
{
    START,
    OPERAND,
    OPERATOR
}
public class EvalInFix
{
    public static void main(String[] args)
    {
        String input = args[0];
        Map<String, Integer> precMap = new HashMap<>();
        precMap.put("+", 1);
        precMap.put("*", 2);
        Stack<String> stack  = new Stack<>();
        State state =  State.START;
        StringBuilder str = new StringBuilder();

        for (int i=0; i < input.length(); i++)
        {
            Character c = input.charAt(i);

            switch (state)
            {
                case START:
                    state = State.OPERAND;
                    str.append(c);
                    break;

                case OPERAND:
                    if (Character.isDigit(c))
                    {
                        str.append(c);
                    }
                    else
                    {
                        String operand = str.toString();
                        stack.push(operand);
                        str.delete(0, operand.length());
                        state = State.OPERATOR;
                        String operator = c.toString();

                        // There is at least another operator in the stack
                        while (stack.size() > 1)
                        {
                            String operand2 = stack.pop();
                            String operator2 = stack.pop();
                            if (precMap.get(operator) <= precMap.get(operator2))
                            {
                                // Evaluate stack
                                String operand1 =  stack.pop();
                                String result = evaluate(operand1, operator2, operand2);
                                stack.push(result);
                            }
                            else
                            {
                                stack.push(operator2);
                                stack.push(operand2);
                                break;
                            }
                        }
                        stack.push(operator);
                    }
                    break;
                case OPERATOR:
                    if (Character.isDigit(c))
                    {
                        str.append(c);
                        state = State.OPERAND;
                    }
                    break;
                default:
            }
        }

        if (state == State.OPERAND)
        {
            stack.push(str.toString());
        }

        while (stack.size() > 1)
        {
            String operand2 = stack.pop();
            String operator = stack.pop();
            String operand1 =  stack.pop();
            String result = evaluate(operand1, operator, operand2);
             stack.push(result);
        }

        if (stack.size() > 0)
        {
            System.out.println(stack.pop());
        }
    }

    private static String evaluate(String operand1, String operator, String operand2)
    {
        if (operator.equals("*"))
        {
            return String.valueOf(Integer.parseInt(operand1) * Integer.parseInt(operand2));
        }
        return String.valueOf(Integer.parseInt(operand1) + Integer.parseInt(operand2));
    }
}
