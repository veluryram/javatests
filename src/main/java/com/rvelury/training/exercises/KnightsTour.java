package com.rvelury.training.exercises;

public class KnightsTour
{
    public static void main(String[] args)
    {
        int[][]  board = new int[8][8];

        solve(board, 0, 0, 0);

    }

    static boolean solve(int[][] board, int col, int row, int move )
    {
        board[col][row] = move;

        return false;

    }
}
