package com.rvelury.training.datastructures.priorityqueue;

public class UnorderedMaxPQ<Key extends Comparable<Key>>
{
    private Key[] pq;
    private int N;

    public UnorderedMaxPQ(int capacity)
    {
        pq = (Key[]) new Comparable[capacity];
    }

    public boolean isEmpty()
    {
        return N == 0;
    }

    public void insert(Key x)
    {
        pq[N++] = x;
    }

    public Key delMax()
    {
        int max=0;
        for (int i =1; i < N; i++)
        {
            if (pq[max].compareTo(pq[i]) < 0) max = i;
        }
        Key tmp = pq[max];
        pq[max] = pq[N-1];
        pq[N-1] = tmp;
        return pq[--N];
    }
}
