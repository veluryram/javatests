package com.rvelury.training.datastructures;

public class MyHashMap<K, V>
{
    private static final int INITIALSIZE = 1024;

    private MapEntry[] hashTable;
    private int tableSize = INITIALSIZE;

    static class MapEntry<K,V>
    {
        private K key;
        private V value;
        MapEntry next;

        MapEntry(K key, V value)
        {
            this.key = key;
            this.value = value;
        }
    }

    MyHashMap()
    {
        hashTable = new MapEntry[1024];
    }

    public static void main(String[] args)
    {
        MyHashMap<String, Integer> myMap = new MyHashMap<>();

        myMap.put("apple", 10);
        myMap.put("orange", 8);
        myMap.put("pear", 12);
        myMap.put("kiwi", 5);

        printPrice("apple", myMap);

        System.out.println(myMap.toString());
    }

    static void printPrice(String fruit, MyHashMap<String, Integer> myMap )
    {
        Integer price;
        price = myMap.get(fruit);
        if (price != null)
        {
            System.out.println("Price of " + fruit + " is " + price);
        }
        else
        {
            System.out.println("Price of " + fruit + " is not specified");
        }
    }

    public V get(K key)
    {
        int hash = Math.abs(key.hashCode()) % tableSize ;

        MapEntry<K,V> e = hashTable[hash];

        while (e != null)
        {
            if (e.key.equals(key))
            {
                return e.value;
            }
            e = e.next;
        }

        return null;
    }

    public V put(K key, V value)
    {
        int hash = Math.abs(key.hashCode()) % tableSize ;

        MapEntry<K,V> prev = null, e = hashTable[hash];

        if (e == null)
        {
            hashTable[hash] = new MapEntry<>(key, value);
            return value;
        }
        else
        {
            while (e != null)
            {
                if (e.key.equals(key))
                {
                    V oldValue = e.value;
                    e.value = value;
                    return oldValue;
                }

                prev = e;
                e = e.next;
            }

            prev.next = new MapEntry<>(key, value);
            return value;
        }
    }

    public String toString()
    {
        StringBuilder s = new StringBuilder();
        int index = 0;
        for (MapEntry entry: hashTable)
        {
            if (entry != null)
            {
                if (index == 0)
                {
                    s.append("[ ");
                }
                else
                {
                    s.append(" , ");
                }
                s.append("<" + entry.key.toString() + "," + entry.value.toString() + ">");
                index++;
            }
        }
        s.append(" ]\n");
        return s.toString();
    }
}
