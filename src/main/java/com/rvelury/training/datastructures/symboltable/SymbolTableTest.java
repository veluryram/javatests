package com.rvelury.training.datastructures.symboltable;

import com.rvelury.training.datastructures.symboltable.MySymbolTable;

public class SymbolTableTest
{
    public static void main(String[] args)
    {
        String[] inputKeys = new String[]{"S","E","A","R","C","H","E","X","A","M","P","L","E"};
        Integer[] inputValues = new Integer[]{0,1,2,3,4,5,6,7,8,9,10,11,12};

        SortedArraySymbolTable<String, Integer> st = new SortedArraySymbolTable<String, Integer>(inputKeys, inputValues);

        /*
        for (int i=0; i < inputKeys.length; i++)
        {
            st.put(inputKeys[i], inputValues[i]);
        }
        */

        System.out.println("Value for key S :" + st.get("S"));
        for (String s: st.keys())
        {
            System.out.println(s + " '" + st.get(s));
        }
    }
}
