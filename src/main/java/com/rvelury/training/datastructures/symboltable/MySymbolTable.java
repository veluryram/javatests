package com.rvelury.training.datastructures.symboltable;

public class MySymbolTable<K,V>
    implements SymbolTable<K, V>
{
    MySymbolTable()
    {

    }

    @Override
    public V get(K key)
    {
        return null;
    }

    @Override
    public void put(K key, V value)
    {

    }

    @Override
    public void delete(K key)
    {

    }

    @Override
    public void contains(K key)
    {

    }

    @Override
    public boolean isEmpty()
    {
        return false;
    }

    @Override
    public int size()
    {
        return 0;
    }

    @Override
    public Iterable<K> keys()
    {
        return null;
    }
}
