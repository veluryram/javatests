package com.rvelury.training.datastructures.symboltable;

import java.util.*;

public class SortedArraySymbolTable<K extends Comparable,V>
    implements SymbolTable<K, V>
{
    int size=0;
    ArrayList<Entry> table = new ArrayList<>();

    class Entry
    {
        K key;
        V value;

        Entry(K key, V value)
        {
            this.key = key;
            this.value = value;
        }

        K getKey()
        {
            return key;
        }
    }

    SortedArraySymbolTable(K[] keys, V[] values)
    {
        assert keys.length == values.length;

        Set<Entry> tmpTable  = new TreeSet<>(Comparator.comparing(Entry::getKey));
        for (int i =0; i < keys.length; i++)
        {
            tmpTable.add(new Entry(keys[i], values[i]));
        }
        for (Entry e: tmpTable)
        {
            table.add(e);
        }
        size = table.size();
    }

    @Override
    public V get(K key)
    {
        if (isEmpty()) return null;
        int i = rank(key);

        if (i < size && table.get(i).key.compareTo(key) == 0) return table.get(i).value;
        else return null;
    }

    @Override
    public void put(K key, V value)
    {

    }

    @Override
    public void delete(K key)
    {

    }

    @Override
    public void contains(K key)
    {

    }

    @Override
    public boolean isEmpty()
    {
        return size == 0;
    }

    @Override
    public int size()
    {
        return 0;
    }

    @Override
    public Iterable<K> keys()
    {
        return () -> new Iterator<K>()
        {
            int current=0;

            @Override
            public boolean hasNext()
            {
                return current < size;
            }

            @Override
            public K next()
            {
                return table.get(current++).key;
            }
        };
    }

    private int rank(K key)
    {
        int lo=0;
        int hi=size-1;

        while (lo < hi)
        {
            int mid = lo + (hi - lo)/2;
            int cmp = key.compareTo(table.get(mid).key);

            if (cmp < 0) hi = mid - 1;
            else if (cmp > 0) lo = mid + 1;
            else return mid;
        }
        return lo;
    }
}
