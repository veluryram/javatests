package com.rvelury.tests.dynprog;

import java.util.*;

public class AdSpend
{
    private static Random random = new Random(23);
    private static int nCategories = 3;
    private static int nItems = 20;
    private static int[][] adCosts = new int[nCategories][nItems];
    private static int [] categoryMins = new int[nCategories];
    private static int [] categoryMaxs = new int[nCategories];
    private static int [] cumulativeMins = new int[nCategories];
    private static int [] cumulativeMaxs = new int[nCategories];
    private static MemoizationMap[] memoizationMaps = new MemoizationMap[nCategories];

    public static void main(String[] args)
    {
        generateData();

        for (int i=0; i < nCategories; i++)
        {
            System.out.println(Arrays.toString(adCosts[i]));
            memoizationMaps[i] = new MemoizationMap();
        }
        System.out.println(Arrays.toString(categoryMins));
        System.out.println(Arrays.toString(categoryMaxs));
        System.out.println(Arrays.toString(cumulativeMins));
        System.out.println(Arrays.toString(cumulativeMaxs));

        System.out.println(getLargestAmountBelow(adCosts[0], 32));
        System.out.println(getLargestAmountBelow(adCosts[0], 1));
        System.out.println(getLargestAmountBelow(adCosts[0], 100));

        int budget = 120;
        if (budget < cumulativeMins[0] + adCosts[0][0])
        {
            System.err.println("Allocation not feasible");
        }

        if (budget > cumulativeMaxs[0] + adCosts[0][nItems-1])
        {
            System.out.println("Trivial Case. Budget accommodates highest costs across all categories");
        }

        int slack = calculateOptimum(0, budget);

        int tmpBudget = budget;
        AllocationDetails allocation;
        for (int i = 0; i < nCategories; i++)
        {
            final int currentCategory = i;
            if (i == 0)
            {
                allocation = memoizationMaps[i].map.get(tmpBudget);
                if (allocation != null)
                {
                    System.out.println("Category: " + i + " (Budget: " + tmpBudget + ", Cost: " + adCosts[i][allocation.item] + ")");
                    tmpBudget -= adCosts[i][allocation.item];
                }
            }
            else if (i == nCategories-1)
            {
                System.out.println("Category: " + i + " (Budget: " + tmpBudget + ", Cost: " + getLargestAmountBelow(adCosts[i], tmpBudget));
            }
            else
            {
                memoizationMaps[currentCategory].map
                        .forEach((k,v) -> System.out.println("Category: " + currentCategory + " (Budget: " + k + ", Cost: " + adCosts[currentCategory][v.item] + ")"));
                allocation = memoizationMaps[i].map.get(tmpBudget);
                if (allocation != null)
                {
                    System.out.println("Category: " + i + " (Budget: " + tmpBudget + ", Cost: " + adCosts[i][allocation.item] + ")");
                    tmpBudget -= adCosts[i][allocation.item];
                }
            }
        }
        System.out.println("Unspend amount: " + slack);
    }

    private static int getLargestAmountBelow(int[] array, int amount)
    {
        int index = Arrays.binarySearch(array, amount);

        if (index >= 0)
        {
            return array[index];
        }
        else {
            if (index == -(array.length+1))
            {
               return 0;
            }
            else
            {
                if (-index -2 >-0)
                {
                    return array[-index-2];
                }
            }
        }
        return 0;
    }

    private static int calculateOptimum(int currentCategory, int budget)
    {
        if (currentCategory >= nCategories-1)
        {
            int allocatedamount = getLargestAmountBelow(adCosts[currentCategory], budget);
            // System.out.println(Arrays.toString(allocationAmounts));
            return budget-allocatedamount;
        }

        AllocationDetails cache;
        if ((cache = memoizationMaps[currentCategory].get(budget)) != null)
        {
            return cache.slack;
        }

        SortedSet<Pair> slackSet = new TreeSet<>();
        int slack;
        for (int i=0; i < nItems; i++)
        {
            int remainingBudget = budget - adCosts[currentCategory][i];
            if (remainingBudget >= cumulativeMins[currentCategory] && remainingBudget <= cumulativeMaxs[currentCategory])
            {
                slack =calculateOptimum(currentCategory+1, remainingBudget);
                slackSet.add(new Pair(slack, i));
            }
        }
        slack = slackSet.first().slack;
        memoizationMaps[currentCategory].add(budget, slack, slackSet.first().index);
        return slack;
    }

    private static void generateData()
    {
        for (int i=0; i < nCategories; i++)
        {
            for (int j=0; j < nItems; j++)
            {
                int cost = random.nextInt(100);
                if (cost == 0)
                {
                    cost = 1;
                }
                adCosts[i][j] = cost;
            }
            Arrays.sort(adCosts[i]);
            categoryMins[i] = adCosts[i][0];
            categoryMaxs[i] = adCosts[i][nItems-1];
        }
        int cumulativeMin = 0;
        int cumulativeMax = 0;
        for (int i=nCategories-1; i>=0; i--)
        {
            cumulativeMins[i] = cumulativeMin;
            cumulativeMin += categoryMins[i];
            cumulativeMaxs[i] = cumulativeMax;
            cumulativeMax += categoryMaxs[i];
        }
    }
}

class MemoizationMap
{
    Map<Integer, AllocationDetails> map = new HashMap<>();

    void add(int budgetAmount, int slack, int itemIndex)
    {
        map.put(budgetAmount, new AllocationDetails(slack, itemIndex));
    }

    AllocationDetails get(int budgetAmount)
    {
        if (map.get(budgetAmount) == null)
        {
            return null;
        }
        else
        {
            return map.get(budgetAmount);
        }
    }
}

class AllocationDetails
{
    int slack;
    int item;

    AllocationDetails(int slack, int item)
    {
        this.slack = slack;
        this.item = item;
    }
}

class Pair
    implements Comparable
{
    Integer slack;
    Integer index;

    Pair(Integer slack, Integer amount)
    {
        this.slack = slack;
        this.index = amount;
    }

    @Override
    public int compareTo(Object o)
    {
        return slack - ((Pair)o).slack;
    }
}
