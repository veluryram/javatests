package com.rvelury.tests.exceptions;

public class FinallyTest
{
    public static void main(String[] args)
    {
        runFinallyTest();
    }

    private static int runFinallyTest()
    {
        try
        {
            System.out.println("Returning");
            return 0;
        }
        finally
        {
            System.out.println("Finally executed");
        }

    }
}
