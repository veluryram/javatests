package com.rvelury.tests.exceptions;

import java.sql.SQLException;

public class ExceptionCastTest<T extends Exception>
{
    public static void main(String[] args)
    {
        try
        {
            new ExceptionCastTest<RuntimeException>().pleaseThrow(new SQLException());
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
        }
    }

    private void pleaseThrow(final Exception t) throws T
    {
        throw (T) t;
    }
}
