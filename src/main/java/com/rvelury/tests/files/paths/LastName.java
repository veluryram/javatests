package com.rvelury.tests.files.paths;

import java.nio.file.Path;
import java.nio.file.Paths;

public class LastName
{
    public static void main(String[] args)
    {
        String pathString = "data/cs/mutable";
        Path path = Paths.get(pathString);

        System.out.println(path.getFileName().toString());
        System.out.println(path.getParent().toString());
    }
}
