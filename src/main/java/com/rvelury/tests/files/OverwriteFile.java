package com.rvelury.tests.files;

import java.io.DataOutputStream;
import java.io.FileOutputStream;

/**
 * Created by Ram Velury on 7/28/17.
 */
public class OverwriteFile
{
    public static void main(String[] args)
    {
        try
        {
            DataOutputStream out = new DataOutputStream(new FileOutputStream("test.txt"));
            out.writeBytes("New");
            out.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
