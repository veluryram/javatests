package com.rvelury.tests.files.reader;

import java.io.BufferedReader;
import java.io.FileReader;

public class BufferedReaderTest
{
    public static void main(String[] args)
    {
        try(BufferedReader reader = new BufferedReader(new FileReader("readertest.txt")))
        {
            String line;
            while ((line = reader.readLine()) != null)
            {
                System.out.println(line);
            }

        }
        catch (Exception e)
        {
            System.out.println("Error reading file " + e.getMessage());
        }
    }
}
