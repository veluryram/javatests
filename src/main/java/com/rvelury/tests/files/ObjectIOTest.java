package com.rvelury.tests.files;

import org.apache.commons.lang3.tuple.ImmutablePair;

import java.io.*;

public class ObjectIOTest
{
    public static void main(String[] args)
    {
        ImmutablePair[] inputPairs = new ImmutablePair[]{
                new ImmutablePair<>(1, 2),
                new ImmutablePair<>(2, 3),
                new ImmutablePair<>(3, 4),
                new ImmutablePair<>(4, 5),
                new ImmutablePair<>(5, 6),
                new ImmutablePair<>(6, 7),
                new ImmutablePair<>(7, 8),
                new ImmutablePair<>(8, 9),
                new ImmutablePair<>(9, 1),
                new ImmutablePair<>(2, 4)
        };

        try
        {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("pairs.dat"));
            for (ImmutablePair pair: inputPairs)
            {
                os.writeObject(pair);
            }

            ObjectInputStream is = new ObjectInputStream(new FileInputStream("pairs.dat"));
            ImmutablePair<Integer, Integer> pair;
            while ((pair = (ImmutablePair<Integer, Integer>) is.readObject()) != null)
            {
                System.out.println("(" +  pair.left + "," + pair.right + ")");
            }
        }
        catch (EOFException ignored)
        {

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
