package com.rvelury.tests.files;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class JavaDiff
{
    public static void main(String[] args) {
        List<String> original = fileToLines("originalFile.txt");
        List<String> revised  = fileToLines("revisedFile.xt");

        // Compute diff. Get the Patch object. Patch is the container for computed deltas.
        Patch patch = DiffUtils.diff(original, revised);

        List<Delta> deltas = patch.getDeltas();

        for (Delta delta: deltas) {
            System.out.println(delta);
        }
    }

    static private List<String> fileToLines(String filename) {
        List<String> lines = new LinkedList<String>();
        String line = "";
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}
