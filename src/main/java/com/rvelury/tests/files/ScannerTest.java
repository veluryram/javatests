package com.rvelury.tests.files;

import java.io.ByteArrayInputStream;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ScannerTest
{
    public static void main(String[] args)
    {
        String input = "4\n" +
                "3\n" +
                "0 1\n" +
                "0 2\n" +
                "2 3\n";

        Scanner scanner = new Scanner(new ByteArrayInputStream(input.getBytes()));

        try
        {
            System.out.println("No. of vertices: " + scanner.nextInt());
            int edgeCount = scanner.nextInt();
            System.out.println("No of edges: " + edgeCount);

            for (int i=0; i < edgeCount; i++)
            {
                int source = scanner.nextInt();
                int target = scanner.nextInt();
                System.out.println("Edge: " + source + " -> " + target);
            }
        }
        catch (InputMismatchException e)
        {
            System.err.println("Invalid input encountered");
        }
        catch (NoSuchElementException e)
        {
            System.out.println("End of input");
        }
    }
}
