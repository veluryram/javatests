package com.rvelury.tests.input;

import jline.console.completer.Completer;
import jline.internal.Configuration;

import java.io.File;
import java.util.List;

import static jline.internal.Preconditions.checkNotNull;

public class MyFileNameCompleter
        implements Completer
{
    // TODO: Handle files with spaces in them

    private static final boolean OS_IS_WINDOWS;

    static {
        String os = Configuration.getOsName();
        OS_IS_WINDOWS = os.contains("windows");
    }

    public int complete(String buffer, final int cursor, final List<CharSequence> candidates) {
        // buffer can be null
        checkNotNull(candidates);

        if (buffer == null) {
            buffer = "";
        }

        if (OS_IS_WINDOWS) {
            buffer = buffer.replace('/', '\\');
        }

        int len = buffer.length()-1;

        if (len > 0)
        {
            while (len > 0 && !Character.isWhitespace(buffer.charAt(len)))
            {
                len--;
            }

            if (Character.isWhitespace(buffer.charAt(len)))
            {
                len++;
            }
        }

        String translated = len >0 ?buffer.substring(len): buffer;

        File homeDir = getUserHome();

        // Special character: ~ maps to the user's home directory
        if (translated.startsWith("~" + separator())) {
            translated = homeDir.getPath() + translated.substring(1);
        }
        else if (translated.startsWith("~")) {
            translated = homeDir.getParentFile().getAbsolutePath();
        }
        else if (!(new File(translated).isAbsolute())) {
            String cwd = getUserDir().getAbsolutePath();
            translated = cwd + separator() + translated;
        }

        File file = new File(translated);
        final File dir;

        if (translated.endsWith(separator())) {
            dir = file;
        }
        else {
            dir = file.getParentFile();
        }

        File[] entries = dir == null ? new File[0] : dir.listFiles();

        if (entries == null) {
            return -1;
        }
        else
        {
            matchFiles(translated, entries, candidates);

            final int index = buffer.lastIndexOf(separator());

            if (index > 0)
            {
                return index + separator().length();
            }
            else
            {
                return len;
            }
        }
    }

    protected String separator() {
        return File.separator;
    }

    protected File getUserHome() {
        return Configuration.getUserHome();
    }

    protected File getUserDir() {
        return new File(".");
    }

    protected void matchFiles(final String translated, final File[] files, final List<CharSequence> candidates) {

        int matches = 0;

        // first pass: just count the matches
        for (File file : files) {
            if (file.getAbsolutePath().startsWith(translated)) {
                matches++;
            }
        }
        for (File file : files) {
            if (file.getAbsolutePath().startsWith(translated)) {
                CharSequence name = file.getName() + (matches == 1 && file.isDirectory() ? separator() : " ");
                candidates.add(render(file, name).toString());
            }
        }
    }

    protected CharSequence render(final File file, final CharSequence name) {
        return name;
    }
}
