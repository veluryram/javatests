package com.rvelury.tests.enums;

import java.io.Serializable;

/**
 * Created by Ram Velury on 7/19/17.
 */
public enum MetaType
        implements Serializable
{

    /**
     * Universe
     */
    UNIVERSE(0),
    /**
     * Galaxy
     */
    GALAXY(1),
    /**
     * Constellation
     */
    CONSTELLATION(2),
    /**
     * Star
     */
    STAR(3),
    /**
     * Attribute
     */
    ATTRIBUTE(4),
    /**
     * Attribute rule
     */
    DEPENDENCY_RULE(5),
    /**
     * Star rule
     */
    STAR_RULE(6),
    /**
     * Equivalence rule
     */
    EQUIVALENCE_RULE(7),
    /**
     * Binding
     */
    BINDING(8),
    /**
     * Source
     */
    SOURCE(9),
    /**
     * Sink
     */
    SINK(10),
    /**
     * Column
     */
    COLUMN(11),
    /**
     * TemporaryId for foreign objects
     */
    FOREIGN(12);

    /**
     * Associated integer value
     */
    private final int value;

    /**
     * Constructs Meta Type
     * @param value Meta Type value
     */
    MetaType(int value)
    {
        this.value = value;
    }

    /**
     * Returns integer value the is associated with the given Meta Type
     *
     * @return Associated integer value
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Returns true if the meta type relates to the context object
     *
     * @return Boolean value
     */
    public boolean isContext()
    {
        switch (this)
        {
            case GALAXY:
            case CONSTELLATION:
            case STAR:
                return true;
            default:
                return false;
        }
    }
}
