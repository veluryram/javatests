package com.rvelury.tests.streams.parallel;

public class IterativeWordCounter
{
    final static String SENTENCE = " Nel   mezzo del cammin  di nostra  vita mi  ritrovai in una  selva oscura ché la  dritta via era   smarrita ";

    public static void main(String[] args)
    {
        System.out.println("Found " + countWordsInteratively(SENTENCE) + " words");
    }

    public static int countWordsInteratively(String s)
    {
        int counter=0;
        boolean lastSpace=true;
        for (char c:s.toCharArray())
        {
            if (Character.isWhitespace(c))
            {
                lastSpace = true;
            }
            else
            {
                if (lastSpace)
                {
                    counter++;
                }
                lastSpace = false;
            }
        }
        return counter;
    }
}
