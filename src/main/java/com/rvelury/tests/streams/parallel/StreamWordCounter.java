package com.rvelury.tests.streams.parallel;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamWordCounter
{
    final static String SENTENCE = " Nel   mezzo del cammin  di nostra  vita mi  ritrovai in una  selva oscura ché la  dritta via era   smarrita ";

    public static void main(String[] args)
    {
        Stream<Character> stream = IntStream.range(0, SENTENCE.length()).mapToObj(SENTENCE::charAt);
        System.out.println("Sequential Word Count: Found " + countWords(stream) + " words");


        stream = IntStream.range(0, SENTENCE.length()).mapToObj(SENTENCE::charAt);
        System.out.println("Parallel Word Count: Found " + countWords(stream.parallel()) + " words");
    }

    private static int countWords(Stream<Character> stream)
    {
        WordCounter wordCounter = stream.reduce(
                new WordCounter(0, true),
                WordCounter::accumulate,
                WordCounter::combine
                );
        return wordCounter.getCounter();
    }
}
