package com.rvelury.tests.features.nested;

public class Outer1
{
    public static int temp1 = 1;
    private static int temp2 = 2;
    public int temp3 = 3;
    private int temp4 = 4;

    public static class Inner
    {
        private static int temp5 = 5;

        private static int getSum()
        {
            //return (temp1 + temp2 + temp3 + temp4 + temp5);
            return (temp1 + temp2 + temp5);
        }
    }

    public static void main(String[] args)
    {
        Inner obj = new Outer1.Inner();
        System.out.println(Inner.getSum());
        System.out.println(obj.getSum());
    }

}

