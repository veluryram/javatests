package com.rvelury.tests.transaction;

import java.io.*;

/**
 * Created by Ram Velury on 7/6/17.
 */
public class TransactionManager
{

    private final static TransactionManager INSTANCE = new TransactionManager();
    private static int transactionId;
    private static final String TRANSACTION_ID_FILE_PATH = "tid.ser";

    private TransactionManager()
    {
    }

    static {
        try
        {
            if (new File(TRANSACTION_ID_FILE_PATH).exists())
            {
                DataInputStream in = new DataInputStream(new FileInputStream(TRANSACTION_ID_FILE_PATH));
                transactionId = in.readInt();
                in.close();

            }
            else
            {
                transactionId = 0;
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Error initializing Transaction Manager");
        }
    }

    public static TransactionManager getInstance()
    {
        return INSTANCE;
    }

    public synchronized int generateTransactionId()
    {
        return ++TransactionManager.transactionId;
    }

    public void close()
    {
        try
        {
            DataOutputStream out = new DataOutputStream(new FileOutputStream(TRANSACTION_ID_FILE_PATH));
            out.writeInt(transactionId);
            out.close();
            System.out.println("Closed Transaction Manager");
        }
        catch (Exception e)
        {
            throw new RuntimeException("Error closing Transaction Manager");
        }
    }
}
