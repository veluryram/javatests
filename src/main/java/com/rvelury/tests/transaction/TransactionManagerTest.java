package com.rvelury.tests.transaction;

import java.util.Scanner;

/**
 * Created by Ram Velury on 7/15/17.
 */

public class TransactionManagerTest
{
    private static TransactionManager tm;

    public static void main( String [] args )
    {
        try
        {
            tm = TransactionManager.getInstance();
            runLoop();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (tm != null)
            {
                tm.close();
            }
        }

    }

    public static void runLoop()
    {
        Scanner scanner = new Scanner(System.in);
        int value = 0;
        try
        {
            while (scanner.hasNextLine())
            {
                scanner.nextLine();
                System.out.println(tm.generateTransactionId());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
