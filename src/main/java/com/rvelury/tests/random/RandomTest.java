package com.rvelury.tests.random;

import java.util.Random;

public class RandomTest
{
    Random random = new Random();
    public static void main(String[] args)
    {
        long seed=23;

        if (args.length > 0)
        {
            seed = Long.parseLong(args[0]);
        }

        RandomTest test = new RandomTest();

        test.setSeed(seed);
        test.print100RandomDoubles();
        test.print100RandomDoublesBetweenZeroAndHundred();

        test.print100RandomInts();
        test.print10RandomInts();

        test.print100GaussianRandomDoubles();
    }

    private void setSeed(long seed)
    {
        random.setSeed(seed);
    }

    private void print100RandomDoubles()
    {
        System.out.println("Print 100 random doubles between 0 and 1");
        random.doubles()
                .limit(100)
                .forEach(System.out::println);

        System.out.println("Print next 100 random doubles between 0 and 1");
        random.doubles(100)
                .forEach(System.out::println);
    }

    private void print100RandomInts()
    {
        System.out.println("Print 100 random integers between 0 and 1000");
        random.ints(100, 0, 1000)
                .forEach(System.out::println);
    }

    private void print10RandomInts()
    {
        for(int i=0; i < 10; i++)
        {
            System.out.println("Next random int: " + random.nextInt(100));
        }
    }

    private void print100RandomDoublesBetweenZeroAndHundred()
    {
        System.out.println("Print 100 random doubles between 0 and 100");
        random.doubles(0, 100)
                .limit(100)
                .forEach(System.out::println);
    }

    private void print100GaussianRandomDoubles()
    {
        System.out.println("Print 100 gaussian random doubles between 0 and 100");
        for(int i=0; i < 100; i++)
        {
            System.out.println("Next random int: " + random.nextGaussian());
        }
    }
}
