package com.rvelury.tests.concurrent.tests.atomicreference;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class AtomicReferenceTest
{
    static AtomicReference<User> userRef = new AtomicReference<User>(new User());

    public static void main(final String[] arguments) throws InterruptedException {

        System.out.println("main thread started");

        List<Thread> listThread = new ArrayList<>();

        for (int i = 1; i <= 1000; i++) {
            listThread.add(new Thread(() -> {

                boolean flag = false;

                while (!flag) {
                    User prevValue = userRef.get();
                    User newValue = new User();
                    newValue.count = prevValue.count + 1;
                    flag = userRef.compareAndSet(prevValue, newValue);
                }

            }));
        }

        for (Thread t : listThread) {
            t.start();
        }

        for (Thread t : listThread) {
            t.join();
        }

        System.out.println("user count:" + userRef.get().count);
        System.out.println("main thread finished");
    }
}
