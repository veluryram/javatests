package com.rvelury.tests.concurrent.tests.addertest;

public interface Counter
{
	public long getCounter();
	public void increment();
}
