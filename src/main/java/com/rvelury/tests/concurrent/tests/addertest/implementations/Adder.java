package com.rvelury.tests.concurrent.tests.addertest.implementations;

import com.rvelury.tests.concurrent.tests.addertest.Counter;

import java.util.concurrent.atomic.LongAdder;

public class Adder implements Counter
{
	private final LongAdder adder = new LongAdder();
	
	public long getCounter()
	{
		return adder.longValue();
	}
	
	public void increment()
	{
		adder.increment();
	}
}
