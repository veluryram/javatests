package com.rvelury.tests.concurrent.tests;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTest
{
    private final AtomicInteger ai;

    public AtomicTest(AtomicInteger atomicInteger)
    {
        this.ai = atomicInteger;
    }

    public void increment()
    {
        int oldVal = ai.get();

        while (!ai.compareAndSet(oldVal, oldVal + 1))
        {
            oldVal = ai.get();
        }
    }

    public int get()
    {
        return ai.get();
    }

    public static void main(String[] args)
    {
        try
        {
            AtomicTest pc = new AtomicTest(new AtomicInteger(5));

            Runnable r1 = () -> {
                pc.increment();
            };

            Runnable r2 = () -> {
                pc.increment();
            };

            Runnable r3 = () -> {
                pc.increment();
            };

            Thread t1 = new Thread(r1);

            Thread t2 = new Thread(r2);

            Thread t3 = new Thread(r3);

            t1.start();
            t2.start();
            t3.start();

            t1.join();
            t2.join();
            t3.join();
            System.out.println(pc.get());
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
