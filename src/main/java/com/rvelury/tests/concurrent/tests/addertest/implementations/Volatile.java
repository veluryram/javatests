package com.rvelury.tests.concurrent.tests.addertest.implementations;


import com.rvelury.tests.concurrent.tests.addertest.Counter;

public class Volatile implements Counter
{
	private volatile long counter;
	
	public long getCounter()
	{
		return counter;
	}
	
	public void increment() 
	{
		++counter;
	}
}
