package com.rvelury.tests.concurrent.tests.atomicreference;

public class MainDriver
{
    public static void main(String[] args)
    {
        try
        {
            for (int i=0; i < 10; i++)
            {
                AtomicReferenceTest.main(new String[]{});
            }
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
