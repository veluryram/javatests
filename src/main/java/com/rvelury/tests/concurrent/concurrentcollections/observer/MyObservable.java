package com.rvelury.tests.concurrent.concurrentcollections.observer;

import java.util.ArrayList;
import java.util.List;

public class MyObservable
{
    List<MyObserver> observers = new ArrayList<>();

    public void addListener(MyObserver  listener)
    {
       observers.add(listener) ;
    }

    public void removeListener(MyObserver  listener)
    {
        observers.remove(listener);
    }

    public void notify(String e)
    {
        synchronized (this)
        {
            for (MyObserver o : observers)
            {
                this.notifyAll();
            }
        }
    }
}
