package com.rvelury.tests.concurrent.concurrentcollections;

public class ConcurrentAccessTest
{
    public static void main(String[] args)
    {
        Runnable r =
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        System.out.println("New thread " + Thread.currentThread().toString());
                    }
                };
        Thread t1 = new Thread(r, "Thread1");
        Thread  t2 = new Thread(r, "Thread2");
        System.out.println(t1.getName());
        System.out.println(t1.getId());
        System.out.println(t2.getName());
        System.out.println(t2.getId());
        t1.run();
        t2.run();
    }
}
