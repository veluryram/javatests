package com.rvelury.tests.concurrent.concurrentcollections.observer;

public class MyObserver
    implements Runnable
{
    String name;
    MyObservable provider;

    public MyObserver(String name, MyObservable provider)
    {
        this.name = name;
        this.provider = provider;
    }

    public void onEvent(String e)
    {
        System.out.println("Got event " + e + " in " + name);
    }

    @Override
    public void run()
    {
        System.out.println("Started thread " + name);
        provider.addListener(this);
        System.out.println("Added myself as listener " + name);
        while(true)
        {
            try
            {
                synchronized (provider)
                {
                    System.out.println("Waiting for notify " + name);
                    provider.wait();
                }
                System.out.println("Got notified " + " in " + name);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

        }
    }
}
