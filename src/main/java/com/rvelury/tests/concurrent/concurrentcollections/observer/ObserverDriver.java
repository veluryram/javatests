package com.rvelury.tests.concurrent.concurrentcollections.observer;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ObserverDriver
{
    public static void main(String[] args)
    {
        MyObservable o = new MyObservable();

         List<MyObserver> observers = IntStream.range(0,10)
            .mapToObj(i -> new MyObserver("Listener"+i, o))
            .collect(Collectors.toList());


        //ExecutorService executorService = Executors.newFixedThreadPool(10);

         for(MyObserver obs: observers)
         {
             new Thread(obs).start();
         }

        o.notify("Event1");
        //executorService.shutdown();
    }
}
