package com.rvelury.tests.concurrent;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ParallelSumTest
{
    static Random random = new Random(23);
    public static void main(String[] args)
    {
        /*
        Stream.iterate(0, n -> n+1)
                .limit(100)
                .map(n -> random.nextInt(100))
                .forEach(System.out::println);
        IntStream.range(0, 100)
                .forEach(System.out::println);
                */

        int[] array = random.ints(100, 0, 100)
                .toArray();
        System.out.println(Arrays.toString(array));

        // Submit sort tasks
        int nTasks = 10;

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(nTasks);
        CountDownLatch latch = new CountDownLatch(nTasks);

        System.out.println("Launching tasks");
        for (int i=0; i < 10; i++)
        {
            SumSubArrayTask task = new SumSubArrayTask(array, i*10, i*10+10, latch);
            executor.execute(task);
        }
        try
        {
            System.out.println("Awaiting completion of tasks");
            latch.await();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        System.out.println("Shutting down executor");
        executor.shutdown();
    }

    static class SumSubArrayTask
        implements Runnable
    {
        private int start;
        private int end;
        private int[] array;
        private CountDownLatch latch;

        SumSubArrayTask(int[] array, int start, int end, CountDownLatch latch)
        {
            this.start = start;
            this.end = end;
            this.array = array;
            this.latch = latch;
        }

        @Override
        public void run()
        {
            int sum=0;
            for (int i=start; i < end; i++)
            {
                sum += array[i];
            }
            System.out.println("Sum from task( " + start + "," + end + ") is " + sum);
            latch.countDown();
        }
    }
}
