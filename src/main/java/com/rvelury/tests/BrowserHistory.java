package com.rvelury.tests;

// Note: You may implement this in python, Clojure or any other language.

// You may also decide to create class or object to wrap this _ your call!

// Browser history: Build the object to support browser history.
// Browser should support URL bar, forward and back buttons
// No need for network calls - this is a data structure only.

// creates the history object _ use a class, record, or other structure if you want!

// EXAMPLE:
// IF:
//  - visit A
//  - visit B
//  - visit C
// THEN:
//   - back yields B, back again yields A.
// THEN:
//   - forward yields B, forward again yields C.
// THEN:
//   - back, visit D eliminates C from the history.

import java.util.ArrayList;
import java.util.List;

public class BrowserHistory {

    // Circular Array of url navigation history
    String[] urlHistory;
    int currentLocation = -1;
    int limit;
    int historyStart = 0;
    int historyEnd = 0;

    // Count to keep track of consecutive back moves
    int consecutiveBackMoveCount = 0;

    // Count to keep track of consecutive forward moves
    int consecutiveForwardMoveCount = 0;

    // creates the history object _ use a class, record, or other structure if you want!
    public BrowserHistory (int limit) {
        this.historyStart=-1;
        this.historyEnd=-1;
        this.currentLocation = -1;
        urlHistory = new String[limit];
    }

    // to support "click a link" on the browser
    public void storeVisit(String url) {
        if (historyStart == -1)
        {
            urlHistory[0] = url;
            historyStart = 0;
            historyEnd = 0;
        }
        else
        {
            if (historyEnd >= historyStart)
            {
                if (historyEnd < limit -1)
                {
                    urlHistory[historyEnd++]  = url;
                }
                else
                {

                }
            }
        }

        if (historyEnd < limit -1)
        {
            urlHistory[historyEnd++]  = url;
        }
        else
        {
            historyEnd = 0;
            if (historyEnd == historyStart)
            {
                historyStart++;
            }
        }
    }

    // to support "go back" on the browser
    public String executeBackMove() {
        if (historyEnd > historyStart)
        {
            // Fresh navigation
            if (currentLocation == -1)
            {
                currentLocation = historyEnd-1;
                return urlHistory[currentLocation];
            }
            else
            {
                if (currentLocation > historyStart)
                {
                    currentLocation--;
                    return urlHistory[currentLocation];
                }
                else
                {
                    return urlHistory[currentLocation];
                }
            }
        }
        else
        {
            // Fresh navigation
            if (currentLocation == -1)
            {
               if (historyEnd > 0)
               {
                   currentLocation = historyEnd-1;
               }
               else
               {
                   currentLocation = limit-1;
               }
               return urlHistory[currentLocation];
            }
            else
            {
                if (currentLocation > 0)
                {
                    return urlHistory[currentLocation];
                }
                else
                {
                    currentLocation = limit-1;
                    if (currentLocation > historyStart)
                    {
                        currentLocation--;
                        return urlHistory[currentLocation];
                    }
                    else
                    {
                        return urlHistory[currentLocation];
                    }
                }
            }

        }
    }

    // to support "go forward" on the browser
    public String executeForwardMove() {
        consecutiveForwardMoveCount++;
        if (urlHistory.length < limit)
        {
            if (currentLocation < urlHistory.length - 1)
            {
                currentLocation++;
            }
        }
        else
        {
            // Handle the case where url history array has become circular
            // Limit consecutive back move count to limit-1
            if (consecutiveForwardMoveCount < limit-1)
            {
                if (currentLocation == limit-1)
                {
                    currentLocation = 0;
                }
                else
                {
                    currentLocation++;
                }
            }
        }
        consecutiveBackMoveCount=0;
        consecutiveForwardMoveCount++;
        return urlHistory[currentLocation];
    }

    // Bonus: look up matching URLs by substring
    public String[] lookup (String sub) {
        List<String> matchedUrls = new ArrayList<>();
        for (String url: urlHistory)
        {
            if (url.indexOf(sub) >= 0)
            {
                matchedUrls.add(url);
            }
        }

        String[] matchedUrlArr = new String[matchedUrls.size()];
        int index = 0;
        for (String url: matchedUrls)
        {
            matchedUrlArr[index++] = url;
        }
        return matchedUrlArr;
    }

    public String currentUrl()
    {
        return urlHistory[currentLocation];
    }
}
