package com.rvelury.tests;

// Note: You may implement this in python, Clojure or any other language.

// You may also decide to create class or object to wrap this _ your call!

// Browser history: Build the object to support browser history.
// Browser should support URL bar, forward and back buttons
// No need for network calls - this is a data structure only.

// creates the history object _ use a class, record, or other structure if you want!

// EXAMPLE:
// IF:
//  - visit A
//  - visit B
//  - visit C
// THEN:
//   - back yields B, back again yields A.
// THEN:
//   - forward yields B, forward again yields C.
// THEN:
//   - back, visit D eliminates C from the history.

import java.util.ArrayList;
import java.util.List;

public class BrowserHistory1
{
    class HistoryNode
    {
        String url;
        HistoryNode next;
        HistoryNode prev;

        HistoryNode(String url)
        {
            this.url = url;
        }
    }

    HistoryNode head;
    HistoryNode tail;
    int historySize=0;
    int limit;
    HistoryNode navigationNode;

    // creates the history object _ use a class, record, or other structure if you want!
    public BrowserHistory1(int limit) {
        this.limit = limit;
    }

    // to support "click a link" on the browser
    public void storeVisit(String url) {
        HistoryNode newVisit = new HistoryNode(url);

        // Empty List
        if (head == null)
        {
            head = newVisit;
            head.next = null;
            head.prev = null;
            tail = head;
            historySize++;
        }
        else
        {
            // If we are in the middle of a navigation reset it
            if (navigationNode != null)
            {
                tail = navigationNode;
                navigationNode = null;
            }

            // Append new visit to end of list
            newVisit.prev = tail;
            tail.next = newVisit;
            tail = newVisit;

            // Handle the case where we exceed the limit
            if (historySize >= limit)
            {
                // Drop head node
                head = head.next;
                head.prev = null;
            }
            historySize++;
        }

    }

    // to support "go back" on the browser
    public String executeBackMove() {
        if (navigationNode == null)
        {
            if (tail != null && tail.prev != null)
            {
                navigationNode = tail.prev;
                return navigationNode.url;
            }
        }
        else
        {
            if (navigationNode.prev != null)
            {
                navigationNode = navigationNode.prev;
                return navigationNode.url;
            }
        }
        return null;
    }

    // to support "go forward" on the browser
    public String executeForwardMove() {
        if (navigationNode != null)
        {
            if (navigationNode.next != null)
            {
                navigationNode = navigationNode.next;
                return navigationNode.url;
            }
        }
        return null;
    }

    // Bonus: look up matching URLs by substring
    public String[] lookup (String sub) {
        List<String> matchedUrls = new ArrayList<>();

        HistoryNode node = head;

        while (node != null)
        {
            if (node.url.indexOf(sub) >= 0)
            {
                matchedUrls.add(node.url);
            }
            node = node.next;
        }

        String[] matchedUrlArr = new String[matchedUrls.size()];
        int index = 0;
        for (String url: matchedUrls)
        {
            matchedUrlArr[index++] = url;
        }
        return matchedUrlArr;
    }
}
