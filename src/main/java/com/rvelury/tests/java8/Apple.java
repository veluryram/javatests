package com.rvelury.tests.java8;

public class Apple
{
    public int getWeight()
    {
        return weight;
    }

    public String getColor()
    {
        return color;
    }

    private int weight;
    private String color;

    public Apple(int weight, String color)
    {
        this.weight = weight;
        this.color = color;
    }

    @Override
    public String toString()
    {
        return "Apple{" +
                "weight=" + weight +
                ", color='" + color + '\'' +
                '}';
    }
}
