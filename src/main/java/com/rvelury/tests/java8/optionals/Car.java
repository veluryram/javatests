package com.rvelury.tests.java8.optionals;

import java.util.Optional;

public class Car
{
    private Insurance insurance;
    public Optional<Insurance> getInsurance() { return Optional.<Insurance>ofNullable(insurance); }
}
