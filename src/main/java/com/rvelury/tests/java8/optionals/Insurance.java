package com.rvelury.tests.java8.optionals;

public class Insurance
{
    public Insurance(String name)
    {
        this.name = name;
    }

    private String name;
    public String getName() { return name; }
}
