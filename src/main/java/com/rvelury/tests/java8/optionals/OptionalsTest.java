package com.rvelury.tests.java8.optionals;

import java.util.Optional;
import java.util.stream.Stream;

public class OptionalsTest
{
    public static void main(String[] args)
    {
        Insurance insurance = new Insurance("Farmers");
        Optional<Insurance> optInsurance = Optional.ofNullable(insurance);
        Optional<String> name = optInsurance.map(Insurance::getName);

        optInsurance.filter(i -> "Cambridge Insurance".equals(i.getName()))
                .ifPresent(i -> System.out.println("ok"));

        Insurance insurance1 = new Insurance("StateFarm");
        Insurance insurance2 = null;
        Optional<Insurance> optInsurance1 = Optional.ofNullable(insurance1);
        Optional<Insurance> optInsurance2 = Optional.ofNullable(insurance2);

        Stream.of(optInsurance1, optInsurance2)
                .filter(Optional::isPresent)
                .filter(i -> "StateFarm".equals(i.get().getName()))
                .forEach(i -> i.ifPresent(j -> System.out.println("Final" + j.getName())));
    }

    public String getCarInsuranceName(Optional<Person> person) {
        return
                person
                .flatMap(Person::getCar)
                .flatMap(Car::getInsurance)
                .map(Insurance::getName)
                .orElse("Unknown");

    }

    public Insurance findCheapestInsurance(Person person, Car car) {
        // queries services provided by the different insurance companies
        // compare all those data
        return null;
    }

    public Optional<Insurance> nullSafeFindCheapestInsurance(
            Optional<Person> person, Optional<Car> car) {
        /*
        if (person.isPresent() && car.isPresent()) {
            return Optional.of(findCheapestInsurance(person.get(), car.get()));
        } else {
            return Optional.empty();
        }
        */

        return person.flatMap(p -> car.map(c -> findCheapestInsurance(p, c)));
    }
}
