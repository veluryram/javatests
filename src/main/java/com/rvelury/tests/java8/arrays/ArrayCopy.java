package com.rvelury.tests.java8.arrays;

public class ArrayCopy
{
    public static void main(String[] args)
    {
        byte[] src = new byte[4096];
        byte[] dest = new byte[8192];

        src[0] = 0;
        src[1] = 1;
        src[2] = 2;
        src[3] = 3;
        System.arraycopy(src, 0, dest, 0, 4096);
        System.out.println(dest[0]);
        System.out.println(dest[1]);
        System.out.println(dest[2]);
        System.out.println(dest[3]);
    }
}
