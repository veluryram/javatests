package com.rvelury.tests.java8.arrays;

import com.rvelury.tests.java8.Apple;

public class JavaArrays
{
    public static void main(String[] args)
    {
        /* Create array of apples */
        Apple[] apples ={
                new Apple(50, "green"),
                new Apple(60, "green"),
                new Apple(70, "red")
        };

        /* Instantiate array */
        int[] intarray;
        intarray = new int[10];

        /* Array literals */
        int[]   ints2 = new int[]{ 1,2,3,4,5,6,7,8,9,10 };

        /* Access array elements */
        System.out.println(ints2[5]);

        /* Array length */
        System.out.println(ints2.length);

        /* Iterate on arrays */
        for (int i : ints2)
        {
            System.out.println(i);
        }

        /* Multi-dimensional arrays. First dimension consists of array
        * of 10 array of ints. Second dimension consists of array of
        * 20 ints*/
        int[][] intArray = new int[10][20];

        int[]   ints = new int[10];
        for(int i=0; i < ints.length; i++){
            ints[i] = 10 - i;
        }
        System.out.println(java.util.Arrays.toString(ints));
        java.util.Arrays.sort(ints);
        System.out.println(java.util.Arrays.toString(ints));

    }
}
