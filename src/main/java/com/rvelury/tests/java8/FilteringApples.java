package com.rvelury.tests.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Class to filter apples that is parametrizable with a predicate
 */
public class FilteringApples
{
    public static void main(String[] args)
    {
        /* Get the list of apples */
        List<Apple> apples = Arrays.asList(
                new Apple(50, "green"),
                new Apple(60, "green"),
                new Apple(70, "red"));

        System.out.println("Heavy Apples\n");
        printApples(filterApples(apples, new AppleHeavyWeightPredicate()));
        System.out.println("\nGreen Apples\n");
        printApples(filterApples(apples, new AppleGreenColorPredicate()));

        System.out.println("\nHeavy Apples with anonymous class\n");
        printApples(filterApples(apples, new ApplePredicate()
        {
            @Override
            public boolean test(Apple a)
            {
                return a.getWeight() >= 70;
            }
        }));

        System.out.println("\nHeavy Apples with lambda\n");
        printApples(filterApples(apples, a -> a.getWeight() >= 70));

        System.out.println("\nAples sorted by weight\n");
        apples.sort((a1, a2) -> a1.getWeight() - a2.getWeight());
        printApples(apples);

        System.out.println("\nSort Apples with Comaparator\n");
        apples.sort(Comparator.comparing(Apple::getWeight).reversed());
        printApples(apples);

    }

    static void printApples(List<Apple> apples)
    {
        for (Apple a: apples)
        {
            System.out.println(a.toString());
        }
    }

    static List<Apple> filterApples(List<Apple> apples, ApplePredicate predicate)
    {
        List<Apple> filteredApples = new ArrayList<>();
        for (Apple a: apples)
        {
            if (predicate.test(a))
            {
                filteredApples.add(a);
            }
        }
        return filteredApples;
    }

    static interface ApplePredicate
    {
        boolean test(Apple a);
    }

    static class AppleHeavyWeightPredicate implements ApplePredicate
    {
        @Override
        public boolean test(Apple a)
        {
            return a.getWeight() >= 60;
        }
    }

    static class AppleGreenColorPredicate  implements ApplePredicate
    {
        @Override
        public boolean test(Apple a)
        {
            return a.getColor().equalsIgnoreCase("green");
        }
    }
}
