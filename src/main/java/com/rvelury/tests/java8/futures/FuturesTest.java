package com.rvelury.tests.java8.futures;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

import static java.util.stream.Collectors.toList;

public class FuturesTest
{
    List<Shop> shops = Arrays.asList(new Shop("BestPrice"),
            new Shop("LetsSaveBig"),
            new Shop("MyFavoriteShop"),
            new Shop("BuyItAll"));

    private final Executor executor =
        Executors.newFixedThreadPool(Math.min(shops.size(), 100),
                new ThreadFactory()
                {
                    @Override
                    public Thread newThread(Runnable r)
                    {
                        Thread t = new Thread(r);
                        t.setDaemon(true);
                        return t;
                    }
                });

    public static void main(String[] args)
    {
        FuturesTest test = new FuturesTest();
        /*
        Shop shop = new Shop();
        long start = System.nanoTime();
        Future<Double> futurePrice = shop.getPriceAsync("milk");
        long invocationTime = ((System.nanoTime() - start) / 1000000);
        System.out.println("Invocation returned after " + invocationTime + " msecs");

        try
        {
            double price = futurePrice.get();
            System.out.printf("Price is %.2f%n", price);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        long retrievalTime = ((System.nanoTime() - start) / 1000000);
        System.out.println("Price returned after " + retrievalTime + " msecs");
        */
        /*
        Nthreads = NCPU * UCPU * (1 + W/C)

        where

        NCPU is the number of cores, available through Runtime.getRuntime().availableProcessors()
        UCPU is the target CPU utilization (between 0 and 1), and
        W/C is the ratio of wait time to compute time
        */

        System.out.println(Runtime.getRuntime().availableProcessors());
        long start = System.nanoTime();
        System.out.println(test.findPrices("myPhone27S"));
        long duration = ((System.nanoTime() - start) / 1000000);
        System.out.println("Done in " + duration + " msecs");


    }

    public static void delay() {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public List<String> findPrices(String product)
    {
        /*
        return shops.parallelStream()
                .map(shop -> String.format("%s price is %.2f",
                        shop.getName(), shop.getPrice(product)))
                .collect(toList());
                */

        List<CompletableFuture<String>> priceFutures =
                shops.stream()
                        .map(shop -> CompletableFuture.supplyAsync(
                                () -> String.format("%s price is %.2f",
                                        shop.getName(), shop.getPrice(product))))
                        .collect(toList());

        return priceFutures.stream()
                .map(CompletableFuture::join)
                .collect(toList());
    }
}
