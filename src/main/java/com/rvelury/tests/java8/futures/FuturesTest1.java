package com.rvelury.tests.java8.futures;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class FuturesTest1
{
    public static void main(String[] args)
    {
            List<Shop> shops = Arrays.asList(new Shop("BestPrice"),
            new Shop("LetsSaveBig"),
            new Shop("MyFavoriteShop"),
            new Shop("BuyItAll"));

            shops.stream()
                    .forEach(System.out::println);

        System.out.println(shops.get(0).getPrice("Milk"));
        CompletableFuture<Double> price = CompletableFuture.supplyAsync(() -> shops.get(0).getPrice("Milk"));
        try
        {
            System.out.println(price.get());
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        catch (ExecutionException e)
        {
            e.printStackTrace();
        }
    }
}
