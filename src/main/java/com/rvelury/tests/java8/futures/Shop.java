package com.rvelury.tests.java8.futures;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import static com.rvelury.tests.java8.futures.FuturesTest.delay;

public class Shop
{
    private Random random = new Random(10);

    public String getName()
    {
        return name;
    }

    String name;

    public Shop(String name)
    {
        this.name = name;
    }

    public double getPrice(String product) {
        return calculatePrice(product);
    }

    public Future<Double> getPriceAsync(String product) {
        CompletableFuture<Double>  futurePrice = new CompletableFuture<>();

        new Thread( () -> {
            try
            {
                double price = calculatePrice(product);
                throw new RuntimeException("Error in getting price");
                //futurePrice.complete(price);
            }
            catch (Exception e)
            {
                futurePrice.completeExceptionally(e);
            }
        }).start();
        return futurePrice;
    }

    public Future<Double> getPriceAsync1(String product)
    {
        return  CompletableFuture.supplyAsync(() -> calculatePrice(product));
    }

    private double calculatePrice(String product) {
        delay();
        return random.nextDouble() * product.charAt(0) + product.charAt(1);
    }

    public String toString()
    {
        return name;
    }
}
