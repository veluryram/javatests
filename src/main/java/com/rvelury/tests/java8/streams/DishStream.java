package com.rvelury.tests.java8.streams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.*;

public class DishStream
{
    public enum CaloricLevel { DIET, NORMAL, FAT }

    public static void main(String[] args)
    {
        List<Dish> menu = Arrays.asList(
                new Dish("pork", false, 800, Dish.Type.MEAT),
                new Dish("beef", false, 700, Dish.Type.MEAT),
                new Dish("chicken", false, 400, Dish.Type.MEAT),
                new Dish("french fries", true, 530, Dish.Type.OTHER),
                new Dish("rice", true, 350, Dish.Type.OTHER),
                new Dish("season fruit", true, 120, Dish.Type.OTHER),
                new Dish("pizza", true, 550, Dish.Type.OTHER),
                new Dish("prawns", false, 300, Dish.Type.FISH),
                new Dish("salmon", false, 450, Dish.Type.FISH) );

        List<String> names =
                menu.stream()
                .filter(d -> {
                    System.out.println("Filtering " + d.getName());
                    return d.getCalories() < 350;
                })
                .map(d -> {
                    System.out.println("Mapping " + d.getName());
                    return d.getName();
                })
                .limit(3)
                .collect(toList());
        System.out.println(names);

        System.out.println("\nVegetarian Dishes\n");
        System.out.println(menu.stream()
                .filter(Dish::isVegetarian)
                .collect(toList())
        );

        /*
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(Arrays.toString(array));

        IntStream.rangeClosed(1, 10).forEach(System.out::println);
        IntStream.rangeClosed(1, 10)
                .filter(n -> n%2 == 0)
                .forEach(System.out::println);

        IntStream.rangeClosed(1, 10)
                .skip(5)
                .forEach(System.out::println);

        Integer[] array1 = {1, 2, 3, 3, 1, 1, 2, 3, 1, 2};
        Arrays.asList(array1)
                .stream()
                .distinct()
                .forEach(System.out::println);

        IntStream.rangeClosed(1, 10)
                .skip(5)
                .forEach(System.out::println);
        List<Integer> dishNames =
                menu
                .stream()
                .map(d -> d.getCalories())
                .collect(toList());
        System.out.println(dishNames);
        List<String> words = Arrays.asList("Java8", "Lambdas", "In", "Action");
        List<Integer> wordSizes =
                words
                .stream()
                .map(String::length)
                .collect(toList());
        System.out.println(wordSizes);

        List<String> words = Arrays.asList("Hello", "World");
        List<String> uniqueLetters = words.stream()
                .map(word -> word.split(""))
                .flatMap(Arrays::stream)
                .distinct()
                .collect(toList());
        System.out.println(uniqueLetters);

        */
        List<Integer> list1 = Arrays.asList(1, 2, 3);
        List<Integer> list2 = Arrays.asList(3, 4);

        list1.stream()
                .flatMap(i -> list2.stream()
                            .map(j -> new int[]{i, j}))
                .forEach(i -> System.out.println(Arrays.toString(i)));
        /*
        String[] str = {"hello", "bye", "ciao", "bye", "ciao"};
        Map<String, Integer> frequencyMap = new HashMap<>();
        Arrays.stream(str)
                .forEach(word -> {
                    if (frequencyMap.get(word) == null)
                    {
                        frequencyMap.put(word, 1);
                    }
                    else
                    {
                        frequencyMap.put(word, frequencyMap.get(word)+1);
                    }
                });

        frequencyMap.entrySet().stream()
                .forEach(e -> System.out.println("(" + e.getKey() + "," + e.getValue() + ")"));

        System.out.println(
                menu.stream()
                .map(d -> 1)
                .reduce(0, Integer::sum)
        );

        System.out.println(
                menu.stream()
                        .count()
        );


        OptionalInt maxCalories = menu.stream()
                .mapToInt(Dish::getCalories)
                .max();

        long howManyDishes = menu.stream().collect(Collectors.counting());
        System.out.println(
        menu.stream()
                .map(Dish::getName)
                .collect(Collectors.joining(", "))
        );

        int totalCalories = menu.stream().collect(reducing(
                0, Dish::getCalories, (i, j) -> i + j));

        Map<Dish.Type, List<Dish>> dishesByType =
                menu.stream().collect(groupingBy(Dish::getType));
        System.out.println(dishesByType);


        Map<CaloricLevel, List<Dish>> dishesByCaloricLevel = menu.stream().collect(
                groupingBy(dish -> {
                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                    else if (dish.getCalories() <= 700) return
                            CaloricLevel.NORMAL;
                    else return CaloricLevel.FAT;
                } ));
        System.out.println(dishesByCaloricLevel);

        Map<Dish.Type, Map<CaloricLevel, List<Dish>>> dishesByTypeCaloricLevel = menu.stream().collect(
                groupingBy(Dish::getType,
                        groupingBy(dish -> {
                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                    else if (dish.getCalories() <= 700) return
                            CaloricLevel.NORMAL;
                    else return CaloricLevel.FAT;
                } )));
        System.out.println(dishesByTypeCaloricLevel);

        Map<Boolean, List<Dish>> partitionedMenu =
                menu.stream().collect(partitioningBy(Dish::isVegetarian));
        System.out.println(partitionedMenu);

        Map<Boolean, Map<Dish.Type, List<Dish>>> vegetarianDishesByType =
                menu.stream().collect(partitioningBy(Dish::isVegetarian, groupingBy(Dish::getType)));
        System.out.println(vegetarianDishesByType);

        Map<Boolean, Dish> mostCaloricPartitionedByVegetarian =
                menu.stream().collect(partitioningBy(Dish::isVegetarian,
                        collectingAndThen(maxBy(comparingInt(Dish::getCalories)), Optional::get)));
        System.out.println(mostCaloricPartitionedByVegetarian);

        Map<Boolean, Map<Boolean, List<Dish>>> list1 =
                menu.stream().collect(partitioningBy(Dish::isVegetarian,
                        partitioningBy(d -> d.getCalories() > 500)));
        System.out.println(list1);
*/

    }
}
