package com.rvelury.tests.java8.streams;

import javafx.util.Pair;

import java.util.stream.Stream;

public class Generator
{
    public static void main(String[] args)
    {
        Stream.iterate(0, n -> n + 2)
                .limit(10)
                .forEach(System.out::println);

        /*
        Stream.iterate(new Pair<Integer,Integer>(0,1), p -> new Pair<Integer,Integer>(p.getValue(), p.getKey()+p.getValue()))
        .limit(10)
        .forEach(p -> System.out.println("("+p.getKey()+","+p.getValue()+")"));
        */

        Stream.iterate(new int[]{0,1}, p -> new int[]{p[1], p[0]+p[1]})
                .limit(10)
                .forEach(p -> System.out.println("("+p[0]+","+p[1]+")"));
    }
}
