package com.rvelury.tests.java8.streams;

import java.util.stream.IntStream;

public class PrimeTest
{
    public static void main(String[] args)
    {
        IntStream.range(1,10)
                .filter(i -> isPrime(i))
                .forEach(System.out::println);

    }

    public static boolean isPrime(int candidate)
    {
        return IntStream.range(2, candidate)
                .noneMatch(i -> candidate%i == 0);
    }
}
