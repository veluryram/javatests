package com.rvelury.tests.java8.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;

public class TransactionTest
{
    public static void main(String[] args)
    {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );

        List<Transaction> transactions1 =
                transactions.stream()
                        .filter(t -> t.getYear() == 2011)
                        .sorted(comparing(Transaction::getValue))
                        .collect(Collectors.toList());
        System.out.println(transactions1);

        List<String> uniqueCities =
                transactions.stream()
                        .map(t -> t.getTrader().getCity())
                        .distinct()
                        .collect(Collectors.toList());
        System.out.println(uniqueCities);

        List<Trader> tradersFromCambridge =
                transactions.stream()
                        .map(Transaction::getTrader)
                        .filter(t -> t.getCity().equalsIgnoreCase("Cambridge"))
                        .distinct()
                        .sorted(comparing(Trader::getName))
                        .collect(Collectors.toList());
        System.out.println(tradersFromCambridge);

        String traderNames =
                transactions.stream()
                        .map(t -> t.getTrader().getName())
                        .distinct()
                        .sorted()
                        .reduce("", (s1, s2) -> s1 + s2);
        System.out.println(traderNames);

        boolean milanTraders =
                transactions.stream()
                        .anyMatch(t -> t.getTrader().getCity().equalsIgnoreCase("Milan"));
        System.out.println(milanTraders);

        Optional<Integer> highestValue =
                transactions.stream()
                        .map(Transaction::getValue)
                        .reduce(Integer::max);
        System.out.println(highestValue);

        Stream<String> stream = Stream.of("Java 8 ", "Lambdas ", "In ", "Action");
        Stream<Transaction> transactionStream = Stream.of(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );
    }
}
