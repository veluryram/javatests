package com.rvelury.tests.java8.streams.parallel;

import java.util.Arrays;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;
import java.util.stream.IntStream;

public class ForkJoinSumCalculator
    extends RecursiveTask<Integer>
{
    int[] numbers;
    int start;
    int end;
    private static final int THRESHOLD=10000;

    ForkJoinSumCalculator(int[] numbers)
    {
        this.numbers = numbers;
        this.start=0;
        this.end=numbers.length-1;
    }

    ForkJoinSumCalculator(int[] numbers, int start, int end)
    {
        this.numbers = numbers;
        this.start=start;
        this.end=end;
    }

    @Override
    protected Integer compute()
    {
        int length=end-start;
        if (length <= THRESHOLD)
        {
            return Arrays.stream(numbers, start, end).sum();
        }

        ForkJoinSumCalculator f1 = new ForkJoinSumCalculator(numbers, start, start+length/2);
        f1.fork();
        ForkJoinSumCalculator f2 = new ForkJoinSumCalculator(numbers, start+length/2, end);
        int f2Result = f2.compute();
        int f1Result = f1.join();

        return f1Result + f2Result;
    }

    public static void main(String[] args)
    {
        int count=100000;
        int[] numbers = IntStream.rangeClosed(1, count).toArray();
        ForkJoinSumCalculator f = new ForkJoinSumCalculator(numbers, 0, count);
        long start = System.currentTimeMillis();
        System.out.println(f.compute());
        long end = System.currentTimeMillis();
        System.out.println("Elapsed time ForkJoinParallel: " + (end-start) + " msecs");

        start = System.currentTimeMillis();
        int sum=0;
        for(int i:numbers)
        {
            sum +=i;
        }
        System.out.println(sum);
        end = System.currentTimeMillis();
        System.out.println("Elapsed time Iterative: " + (end-start) + " msecs");

        start = System.currentTimeMillis();
        System.out.println(Arrays.stream(numbers).parallel().sum());
        System.out.println("Elapsed time ParallelStream: " + (end-start) + " msecs");
    }
}
