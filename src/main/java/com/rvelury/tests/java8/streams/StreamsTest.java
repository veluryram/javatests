package com.rvelury.tests.java8.streams;

import java.util.*;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.Math.sqrt;

public class StreamsTest
{
    public static void main(String[] args)
    {
        /*
        Integer[] intArray = new Integer[] {3,2,1};

        Arrays.stream(intArray).forEach(System.out::println);

        List<Integer> intArray1 = Arrays.asList(intArray);
        intArray1.stream()
                .forEach(System.out::println);
        Comparator<Integer> comp = (i1, i2) -> (i1 -i2);
        intArray1.stream()
                .sorted(Integer::compareTo)
                .forEach(System.out::println);
        intArray1.stream()
                .sorted(comp)
                .forEach(System.out::println);

        Map<String, Integer> map = new HashMap<>();
        map.put("Integer", 1);
        map.put("Float", 2);
        map.put("Double", 3);
        map.entrySet().stream()
                .forEach((e) -> System.out.println("Key: " + e.getKey() + ", Value: " + e.getValue() ));
        List<String> types = map.entrySet().stream()
                .map(Map.Entry::getKey)
                .distinct()
                .collect(Collectors.toCollection(ArrayList::new));
        System.out.println(types);

        Stream.iterate(0, (i) -> i+1)
                .limit(10)
                .forEach(System.out::println);

        System.out.println(IntStream.range(11, 20)
                .sum());
                */

        String str = "Some collection implementations have restrictions on the elements that they may contain. For example, some implementations prohibit null elements, and some have restrictions on the types of their elements. Attempting to add an ineligible element throws an unchecked exception, typically NullPointerException or ClassCastException. Attempting to query the presence of an ineligible element may throw an exception, or it may simply return false; some implementations will exhibit the former behavior and some will exhibit the latter. More generally, attempting an operation on an ineligible element whose completion would not result in the insertion of an ineligible element into the collection may throw an exception or it may succeed, at the option of the implementation. Such exceptions are marked as \"optional\" in the specification for this interface.";

        String[] strArray = str.split(" ");
        String str1 = Arrays.stream(strArray)
                .reduce("", String::concat);
        System.out.println(str1);
        /*
        String str2 = Arrays.stream(strArray)
                .collect(Collectors.joining(",", "(", ")"));
        System.out.println(str2);
        */
        String str2 = Arrays.stream(strArray)
                .collect(Collectors.joining());
        System.out.println(str2);

        IntPredicate isPrime = StreamsTest::checkIfPrime;
        IntStream.range(1, 100)
                .filter(isPrime)
                .forEach(System.out::println);
    }

    public static boolean checkIfPrime(int i)
    {
        for (int j=2; j <= sqrt(i); j++)
        {
            if (i % j == 0)
            {
               return false;
            }
        }
        return true;
    }
}
