package com.rvelury.tests.java8.streams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.exit;
import static java.util.stream.Collectors.toList;

public class FileWordCount
{
    public static void main(String[] args)
    {
        if (args.length < 1)
        {
            exit(1);
        }

        try
        {
            List<String> wordList =
                    Files.lines(Paths.get(args[0]))
                    .flatMap(line -> Arrays.stream(line.split("\\s+")))
                    .distinct()
                    .collect(toList());
            System.out.println(wordList);
        }
        catch (IOException e)
        {
            System.out.println("Error opening file " + args[0]);
        }
    }
}
