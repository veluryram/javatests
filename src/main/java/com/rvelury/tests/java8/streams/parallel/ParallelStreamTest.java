package com.rvelury.tests.java8.streams.parallel;

import java.util.stream.LongStream;
import java.util.stream.Stream;

public class ParallelStreamTest
{
    public static void main(String[] args)
    {
        /*
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "12");
        long start = System.nanoTime();
        System.out.println(sequentialSum(10000));
        long end1 = System.nanoTime();
        System.out.println("Sequential sum: " + (end1-start)/1000000 + " msecs");
        System.out.println(parallelSum(10000));
        long end2 = System.nanoTime();
        System.out.println("Sequential sum: " + (end2-end1)/1000000 + " msecs");
        */
        long start = System.nanoTime();
        System.out.println(rangedSum(1000));
        long end1 = System.nanoTime();
        System.out.println("Sequential sum: " + (end1-start)/1000000 + " msecs");
        System.out.println(rangedSumParallel(10000));
        long end2 = System.nanoTime();
        System.out.println("Parallel sum: " + (end2-end1)/1000000 + " msecs");
    }

    private static Long sequentialSum(long n)
    {
        return Stream.iterate(1L, i -> i + 1 )
                .limit(n)
                .reduce(0L, Long::sum);
    }

    private static Long parallelSum(long n)
    {
        return Stream.iterate(1L, i -> i + 1 )
                .limit(n)
                .parallel()
                .reduce(0L, Long::sum);
    }

    public static long rangedSum(long n) {
        return LongStream.rangeClosed(1, n)
                .reduce(0L, Long::sum);
    }

    public static long rangedSumParallel(long n) {
        return LongStream.rangeClosed(1, n)
                .parallel()
                .reduce(0L, Long::sum);
    }
}
