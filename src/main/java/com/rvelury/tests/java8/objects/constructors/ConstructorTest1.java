package com.rvelury.tests.java8.objects.constructors;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

public class ConstructorTest1
{
    int i;

    public ConstructorTest1(int i)
    {
        this.i = i;
    }

    public static void main(String[] args)
    {
        ConstructorTest1 c1 = new ConstructorTest1(5);
        ConstructorTest1 c2 = c1;
        System.out.println(c1);
        System.out.println(c2);
    }
}
