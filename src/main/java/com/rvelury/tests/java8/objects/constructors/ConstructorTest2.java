package com.rvelury.tests.java8.objects.constructors;

public class ConstructorTest2
{
    int i;

    public ConstructorTest2(int i)
    {
        this.i = i;
        return;
    }

    public static void main(String[] args)
    {
        ConstructorTest2 c1 = new ConstructorTest2(5);
        System.out.println(c1);
    }
}
