package com.rvelury.tests.java8.objects;

import java.util.Objects;

public class ObjectIsNullTest
{
    public static void main(String[] args)
    {
        String a = "hello";

        if (Objects.nonNull(a))
        {
            System.out.println(a);
        }
    }
}
