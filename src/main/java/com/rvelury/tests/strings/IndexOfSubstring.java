package com.rvelury.tests.strings;

/**
 * Created by Ram Velury on 5/23/17.
 */
public class IndexOfSubstring
{
    public static void main(String[] args)
    {
        String string2 =  "components of your bicycle.";
        String string1 =  "bicycle";
        System.out.println(string2.indexOf(string1));
        System.out.println(string2.substring(11).indexOf(string1));
    }
}
