package com.rvelury.tests.strings;

/**
 * Created by Ram Velury on 5/20/16.
 */
public class PrintNullString
{
    static String value = null;

    public static void main(String[] args)
    {
        System.out.println(getString());
    }

    public static String getString()
    {
        return "\"" + value + "\"";
    }
}
