package com.rvelury.tests.strings.encoding;

import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DefaultEncoding
{
    public static void main(String[] args)
    {
        DefaultEncoding encodingTest = new DefaultEncoding();

        System.out.println(Charset.defaultCharset().displayName());

        String input = "The façade pattern is a software design pattern.";
        try
        {
            System.out.println(encodingTest.decodeText(input, "US-ASCII"));
            System.out.println(encodingTest.decodeText(input, "UTF-8"));
            System.out.println(encodingTest.convertToBinary("T", "US-ASCII"));
            System.out.println(encodingTest.convertToBinary("語", "BIG5"));
            String encoded = URLEncoder.encode(input, "US-ASCII");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    String decodeText(String input, String encoding) throws IOException
    {
        return
                new BufferedReader(
                        new InputStreamReader(
                                new ByteArrayInputStream(input.getBytes()),
                                Charset.forName(encoding)))
                        .readLine();
    }

    String convertToBinary(String input, String encoding)
            throws UnsupportedEncodingException
    {
        byte[] encoded_input = Charset.forName(encoding)
                .encode(input)
                .array();
        return IntStream.range(0, encoded_input.length)
                .map(i -> encoded_input[i])
                .mapToObj(e -> Integer.toBinaryString(e ^ 255))
                .map(e -> String.format("%1$" + Byte.SIZE + "s", e).replace(" ", "0"))
                .collect(Collectors.joining(" "));
    }
}
