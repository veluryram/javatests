package com.rvelury.tests.strings;

import java.nio.charset.StandardCharsets;

public class StringBytes
{
    public static void main(String[] args) {

        String example = "This is raw text!";
        byte[] bytes = example.getBytes();

        System.out.println("Text : " + example);
        System.out.println("Text [Byte Format] : " + bytes);
        // no, don't do this, it returns the address of the object in memory
        System.out.println("Text [Byte Format] : " + bytes.toString());

        // convert bytes[] to string
        String s = new String(bytes, StandardCharsets.UTF_8);
        System.out.println("Output : " + s);

        // UnsupportedEncodingException
        //String s = new String(bytes, "UTF_8");

    }
}


