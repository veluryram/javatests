package com.rvelury.tests.strings;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by Ram Velury on 6/30/16.
 */
public class PrintToStringBuffer
{
    public static void main(String[] args)
    {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bout);

        out.println("Testing ByteOutputStream");

        System.out.println(bout.toString());
    }
}
