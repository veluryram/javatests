package com.rvelury.tests.strings.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ram Velury on 5/26/17.
 */
public class SqlRegex
{
    public static void main(String[] args)
    {
        String line = "This order was placed for QT3000! OK?";
        String sqlPattern = "%lac__ %";
        String pattern = sqlRegexToJavaRegex(sqlPattern);
        System.out.println("Sql pattern: " + sqlPattern);
        System.out.println("Java pattern: " + pattern);

        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);

        // Now create matcher object.
        Matcher m = r.matcher(line);
        if (m.find( )) {
            System.out.println("Start index: " + m.start());
            System.out.println("End index: " + m.end());
        }else {
            System.out.println("NO MATCH");
        }
    }

    static String sqlRegexToJavaRegex(String source)
    {
        StringBuilder target = new StringBuilder();

        for(int i=0; i < source.length(); i++)
        {
            if (source.charAt(i) == '_')
            {
                target.append('.');
            }
            else if (source.charAt(i) == '%')
            {
                target.append(".*");
            }
            else
            {
                target.append(source.charAt(i));
            }
        }

        return target.toString();
    }
}
