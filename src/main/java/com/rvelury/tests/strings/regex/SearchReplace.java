package com.rvelury.tests.strings.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchReplace
{
    public static void main(String[] args)
    {
        test1();
        //test2();
        //test3();
    }

    public static void test1()
    {
        String expectedOutput =
                "15                       \n" +
                        "15                       \n" +
                        "1 rows in 5 ms\n" +
                        "\n" +
                        "\n";

        String regex = "rows in [0-9]+ ms";
        //Pattern pattern = Pattern.compile(regex);
        //String p1 = "[0-9]+";
        //Pattern pattern = Pattern.compile(p1);
        //Matcher m = pattern.matcher(expectedOutput);
        //System.out.println(m.find());
        //System.out.println(m.replaceFirst(""));
        //System.out.println(expectedOutput);
        //expectedOutput.replaceFirst("rows in [0-9]+ ms", "");
        //System.out.println(expectedOutput);
        //StringUtils.replaceFirst(expectedOutput, regex, "");
        //System.out.println(expectedOutput);
        expectedOutput = expectedOutput.replaceFirst(regex, "");
        System.out.println(expectedOutput);
    }

    public static void test2()
    {
        String expectedOutput = "WITH\n" +
                "    connector=file:delimited\n" +
                "    delimiter=\"|\"\n" +
                "    skipLines=0\n" +
                "    url=../cascade-benchmark/ssb/mysql/ssb1/lineorder\n" +
                "    WITH  PARTITION_DRIVER\n" +
                "        WITH\n" +
                "            partitionkind=FILE\n" +
                "            partitions=0\n" +
                "            url=../cascade-benchmark/ssb/mysql/ssb1/lineorder;";

        String regex = "cascade-benchmark";
        String newString = expectedOutput.replaceAll(regex, "");
    }

    public static void test3()
    {
        String expectedOutput = "outputfile:            load-otp-data-remote-1m-4.out\n" +
                "timing enabled:        off\n" +
                "running script \"/home/velury/proj/cascade/cascade-client/target/test-classes/ddl-scripts/otp/load-otp-data-remote-1m-4.dml\"\n" +
                "\n" +
                "running ddl command   1: DEPLOY STAR otp \n";

        String regex = "running script .*\n";
        System.out.println(expectedOutput.replaceAll(regex, ""));
    }
}
