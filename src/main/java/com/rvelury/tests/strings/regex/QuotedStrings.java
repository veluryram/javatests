package com.rvelury.tests.strings.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ram Velury on 3/8/17.
 */
public class QuotedStrings
{
    public static void main(String[] args)
    {
        //String text = "\"1 2 \\\"333 4\\\" 55 6    \\\"77\\\" 8 999\"";
        String text = "\"abc\",\"cde\",\"efg\")";
        // 1 2 "333 4" 55 6    "77" 8 999

        String regex = "\"([^\"]*)\"|(\\S+)";

        /*
        while (m.find()) {
            if (m.group(1) != null) {
                System.out.println("Quoted [" + m.group(1) + "]");
            } else {
                System.out.println("Plain [" + m.group(2) + "]");
            }
        }
        */
        boolean matchExists = true;
        //Pattern pattern = Pattern.compile("\"(([^\\\\\"]|\\\\\"|\\\\(?!\"))*)\"");
        Pattern pattern = Pattern.compile("\"(([^\\\\\"]|\\\\\"|\\\\(?!\"))*)\"(,|\\))");
        while (matchExists)
        {
            System.out.println(text);
            Matcher m = pattern.matcher(text);
            if (m.find())
            {
                System.out.println(m.start());
                System.out.println(m.end());
            }
            text = text.substring(m.end());
        }
    }
}
