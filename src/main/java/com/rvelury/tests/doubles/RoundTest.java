package com.rvelury.tests.doubles;

import java.text.DecimalFormat;

/**
 * Created by Ram Velury on 6/28/16.
 */
public class RoundTest
{
    public static void main(String[] args)
    {
        double [] d1array = {
                3.94,
                3.994,
                3.9994,
                3.99994,
                3.999994,
                3.9999994,
                3.99999994,
                3.999999994,
                3.9999999994,
        };

        double [] d2array = {
                12345.94,
                12345.994,
                12345.9994,
                12345.99994,
                12345.999994,
                12345.9999994,
                12345.99999994,
                12345.999999994,
                12345.9999999994,
        };

        DecimalFormat df = new DecimalFormat(".00");

        for (double d: d1array)
        {
            System.out.println("Original Value : " + d);
            System.out.println("Original Value : " + df.format(d));
        }

        for (double d: d2array)
        {
            System.out.println("Original Value : " + d);
            System.out.println("Original Value : " + df.format(d));
        }

        double d = 12345.94 * 12345.94 * 12345.94;
        System.out.println("Original Value : " + d);
        System.out.println("Original Value : " + df.format(d));

    }
}
