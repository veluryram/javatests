package com.rvelury.tests.diffutils;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class DiffFilesUtils
{

    public static boolean compareFiles(String file1, String file2, String diffFile)
            throws IOException
    {
        List<String> original = fileToLines(file1);
        List<String> revised = fileToLines(file2);
        PrintStream out = new PrintStream(new File(diffFile));

        // Compute diff. Get the Patch object. Patch is the container for computed deltas.
        Patch patch = DiffUtils.diff(original, revised);

        if (patch.getDeltas().size() == 0)
        {
            return true;
        }
        else
        {
            List<Delta> deltas = patch.getDeltas();
            for (Delta delta : deltas)
            {
                out.println(delta);
            }
            return false;
        }
    }

    public static void main(String[] args)
    {
        List<String> original = null;
        try
        {
            if (compareFiles("originalFile.txt", "revisedFile.txt", "File.dif"))
            {
                System.out.println("No differences found");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static List<String> fileToLines(String filename)
            throws IOException
    {
        List<String> lines = new LinkedList<String>();
        String line = "";
        BufferedReader in = null;
        try
        {
            in = new BufferedReader(new FileReader(filename));
            while ((line = in.readLine()) != null)
            {
                lines.add(line);
            }
        }
        catch (IOException e)
        {
            throw e;
        }
        finally
        {
            if (in != null)
            {
                try
                {
                    in.close();
                }
                catch (IOException e)
                {
                    // ignore ... any errors should already have been
                    // reported via an IOException from the final flush.
                }
            }
        }
        return lines;
    }
}
