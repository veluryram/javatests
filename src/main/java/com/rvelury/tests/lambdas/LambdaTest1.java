package com.rvelury.tests.lambdas;

public class LambdaTest1
{
   public static void main(String[] args)
   {
      FuncInterfaceA a = () -> System.out.println("A");
      FuncInterfaceB b = () -> System.out.println("B");
   }
}
