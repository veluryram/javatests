package com.rvelury.tests.optional.compilechecks;

public class ObectFunctionalInterfaceTest
{
    public static void main(String[] args)
    {
        Object o;
        Runnable r = new Runnable() {

            @Override
            public void run()
            {
                System.out.println("Running");
            }
        };
        o = r;
        ((Runnable) o).run();
    }
}
