package com.rvelury.tests.optional.compilechecks;

public class EffectiveFinalInLambda
{
    public static void main(String[] args)
    {
        int portNumber = 10;
        Runnable r = () -> System.out.println("Port Number: " + portNumber);
        // portNumber = 20;   // Compilation error if this line is not commented out
    }
}
