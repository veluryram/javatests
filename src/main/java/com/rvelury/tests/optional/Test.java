package com.rvelury.tests.optional;

public class Test
{
    public static void main(String[] args)
    {
        test1();
        test2();
        test3();
    }

    private static void test1()
    {
        Insurance i  = new Insurance("Farmers");
        Car c = new Car();
        c.setInsurance(i);
        Person p = new Person();
        p.setCar(c);

        System.out.println("Test1 result: " + getInsuranceName(p));
    }

    private static void test2()
    {
        Insurance i  = new Insurance("Farmers");
        Car c = new Car();
        Person p = new Person();
        p.setCar(c);

        System.out.println("Test2 result: " + getInsuranceName(p));
    }

    private static void test3()
    {
        Insurance i  = new Insurance("Farmers");
        Car c = new Car();
        Person p = new Person();

        System.out.println("Test3 result: " + getInsuranceName(p));
    }

    private static String getInsuranceName(Person p)
    {
        return p.getCar()
                        .flatMap(Car::getInsurance)
                        .map(Insurance::getName).orElse("Unknown");
    }
}
