package com.rvelury.tests.optional;

public class Insurance
{
    private String name;

    private Insurance()
    {

    }

    public Insurance(String name)
    {
        this.name = name;
    }


    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
