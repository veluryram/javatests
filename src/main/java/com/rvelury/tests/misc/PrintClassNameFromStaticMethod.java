package com.rvelury.tests.misc;

import java.lang.invoke.MethodHandles;

public class PrintClassNameFromStaticMethod
{
    public static void main(String[] args)
    {
        System.out.println(MethodHandles.lookup().lookupClass().getSimpleName());
    }
}
