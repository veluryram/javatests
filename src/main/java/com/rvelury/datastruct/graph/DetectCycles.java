package com.rvelury.datastruct.graph;

import java.io.ByteArrayInputStream;

public class DetectCycles
{
    public static void main(String[] args)
    {
        DetectCycles test = new DetectCycles();
        String input = "4\n" +
                "3\n" +
                "0 1\n" +
                "0 2\n" +
                "2 3\n";

        Graph g = Graph.readGraph(new ByteArrayInputStream(input.getBytes()));
        System.out.println(g);
        System.out.println(test.hasCycles(g));

        input = "4\n" +
                "4\n" +
                "0 1\n" +
                "0 2\n" +
                "1 3\n" +
                "2 3\n";

        g = Graph.readGraph(new ByteArrayInputStream(input.getBytes()));
        System.out.println(g);
        System.out.println(test.hasCycles(g));

        input = "2\n" +
                "2\n" +
                "0 1\n" +
                "1 0\n";

        g = Graph.readGraph(new ByteArrayInputStream(input.getBytes()));
        System.out.println(g);
        System.out.println(test.hasCycles(g));

        input = "3\n" +
                "3\n" +
                "0 1\n" +
                "1 2\n" +
                "2 0\n";

        g = Graph.readGraph(new ByteArrayInputStream(input.getBytes()));
        System.out.println(g);
        System.out.println(test.hasCycles(g));

        input = "1\n" +
                "1\n" +
                "0 0\n";

        g = Graph.readGraph(new ByteArrayInputStream(input.getBytes()));
        System.out.println(g);
        System.out.println(test.hasCycles(g));

        input = "4\n" +
                "6\n" +
                "0 1\n" +
                "0 2\n" +
                "1 2\n" +
                "2 0\n" +
                "2 3\n" +
                "3 3\n";

        g = Graph.readGraph(new ByteArrayInputStream(input.getBytes()));
        System.out.println(g);
        System.out.println(test.hasCycles(g));
    }

    public void dfs(Graph g)
    {
        if (!g.isEmpty())
        {
            boolean[] visited = new boolean[g.getnVertices()];
            dfs(g, 0, visited);
        }
    }

    public void dfs(Graph g, int vertex, boolean[] visited)
    {
        if (visited[vertex])
            return;

        visited[vertex] = true;
        System.out.println(vertex);
        for (int child:  g.getChildren(vertex))
        {
            dfs(g, child, visited);
        }
    }

    public boolean hasCycles(Graph g)
    {
        if (!g.isEmpty())
        {
            boolean[] visited = new boolean[g.getnVertices()];
            boolean[] currentpath = new boolean[g.getnVertices()];

            for (int i = 0; i < g.getnVertices(); i++)
            {
                if (!visited[i])
                    return hasCycles(g, 0, visited, currentpath);
            }
        }
        return false;
    }

    public boolean hasCycles(Graph g, int vertex, boolean[] visited, boolean[] currentpath)
    {
        if (currentpath[vertex])
        {
            return true;
        }

        if (visited[vertex])
        {
            return false;
        }
        visited[vertex] = true;
        currentpath[vertex] = true;
        for (int child: g.getChildren(vertex))
        {
            if (hasCycles(g, child, visited, currentpath))
                return true;
        }
        currentpath[vertex] = false;
        return false;
    }
}
