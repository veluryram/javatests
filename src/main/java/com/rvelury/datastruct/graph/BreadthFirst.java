package com.rvelury.datastruct.graph;

import java.io.ByteArrayInputStream;
import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirst
{
    public static void main(String[] args)
    {
        BreadthFirst test = new BreadthFirst();
        String input = "4\n" +
                "4\n" +
                "0 1\n" +
                "0 2\n" +
                "1 3\n" +
                "2 3\n";

        Graph g = Graph.readGraph(new ByteArrayInputStream(input.getBytes()));
        System.out.println(g);
        test.bfs(g);

        input = "6\n" +
                "8\n" +
                "0 1\n" +
                "0 2\n" +
                "1 3\n" +
                "1 4\n" +
                "2 4\n" +
                "3 4\n" +
                "3 5\n" +
                "4 5\n";

        g = Graph.readGraph(new ByteArrayInputStream(input.getBytes()));
        System.out.println(g);
        test.bfs(g);
    }

    public void bfs(Graph g)
    {
        boolean[] visited = new boolean[g.getnVertices()];

        Queue<Integer> queue = new LinkedList<>();

        for (int i=0; i < g.getnVertices(); i++)
        {
            if (!visited[i])
            {
                queue.add(i);

                while (!queue.isEmpty())
                {
                    int v = queue.poll();

                    if (!visited[v])
                    {
                        visited[v] = true;
                        System.out.println(v);

                        for (Integer adj : g.getChildren(v))
                        {
                            queue.offer(adj);
                        }
                    }
                }
            }
        }
    }
}
