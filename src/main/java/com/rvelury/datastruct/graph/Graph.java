package com.rvelury.datastruct.graph;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.*;

class Graph
{
    private int nVertices;
    private List<List<Integer>> adj;

    public Graph(int nVertices)
    {
        this.nVertices = nVertices;
        adj = new ArrayList<>();
        for (int i=0; i < nVertices; i++)
        {
            adj.add(new ArrayList<>());
        }
    }

    public int getnVertices()
    {
        return nVertices;
    }

    public static Graph readGraph(InputStream stream)
    {
        Scanner scanner = new Scanner(stream);
        Graph g=null;

        try
        {
            int nVertices = scanner.nextInt();
            g = new Graph(nVertices);
            int edgeCount = scanner.nextInt();

            for (int i = 0; i < edgeCount; i++)
            {
                int source = scanner.nextInt();
                int target = scanner.nextInt();
                g.addEdge(source, target);
            }
        }
        catch (InputMismatchException e)
        {
            System.err.println("Invalid input encountered");
        }
        catch (NoSuchElementException e)
        {
            System.out.println("End of input");
        }

        return g == null ? new Graph(0): g;
    }

    public void setnVertices(int nVertices)
    {
        this.nVertices = nVertices;
    }

    void addEdge(int source, int target)
    {
        adj.get(source).add(target);
    }

    public String toString()
    {
        ByteOutputStream bytes = new ByteOutputStream();
        PrintStream str = new PrintStream(bytes);
        str.println("No. of vertices: " + nVertices);
        int edgeCount = 0;

        for (int i=0; i < nVertices; i++)
        {
            for (Integer j: adj.get(i))
            {
                str.println("Edge: " + i + " -> " + j);
                edgeCount++;
            }
        }
        str.println("No of edges: " + edgeCount);
        str.flush();
        return bytes.toString();
    }

    boolean isEmpty()
    {
        return nVertices == 0;
    }

    List<Integer> getChildren(int vertex)
    {
        return adj.get(vertex);
    }
}
